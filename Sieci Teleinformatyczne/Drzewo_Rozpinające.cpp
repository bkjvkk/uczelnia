#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm> //sortowanie wektora
#include <fstream>
#include <set>

using namespace std;

struct Krawedz{
	int lewo;
	int prawo;
	int waga;
	Krawedz(int lewo, int prawo, int waga) : lewo(lewo), prawo(prawo), waga(waga) {};
	Krawedz() { lewo = 0; prawo = 0; waga = 0; };

	friend bool operator<(const Krawedz& lewo, const Krawedz& prawo) {
		return lewo.waga < prawo.waga;
	}
	friend bool operator>(const Krawedz& lewo, const Krawedz& prawo) {
		return lewo.waga > prawo.waga;
	}
	friend bool operator<=(const Krawedz& lhs, const Krawedz& rhs) { return !(lhs > rhs); }
	friend bool operator>=(const Krawedz& lhs, const Krawedz& rhs) { return !(lhs < rhs); }
	friend bool operator==(const Krawedz& lewo, const Krawedz& prawo) { return (lewo.lewo == prawo.lewo && lewo.prawo == prawo.prawo && lewo.waga == prawo.waga); }
	friend bool operator!=(const Krawedz& lewo, const Krawedz& prawo) { return !(lewo == prawo); }
};

void WczytajPlik(string fileName, vector<Krawedz> &krawedzie, set<int> &wierzcholki) {
	fstream file;
	file.open(fileName, ios::in);
	if (file.good()) {
		krawedzie.push_back(Krawedz());
		while (file >> krawedzie.back().lewo && file >> krawedzie.back().prawo && file >> krawedzie.back().waga) {
			wierzcholki.insert(krawedzie.back().lewo);
			wierzcholki.insert(krawedzie.back().prawo);
			krawedzie.push_back(Krawedz());
		}
		krawedzie.pop_back();
	}
	else {
		cout << "cos poszlo nie tak z plikiem XD\n";
	}
	file.close();
}

int ObliczSumeDrzewa(vector<Krawedz> drzewo) {
	int suma = 0;
	for (auto x : drzewo) {
		suma += x.waga;
	}
	return suma;
}

vector<Krawedz> Kruskal(vector<Krawedz> krawedzie, set<int> wierzcholki) {
	vector<Krawedz> wynik;
	vector<int> polaczone;
	set<int> dostepne = wierzcholki;
	sort(krawedzie.begin(), krawedzie.end());
	for (auto x : krawedzie) {
		if (dostepne.find(x.lewo) != dostepne.end() || dostepne.find(x.prawo) != dostepne.end()) {
			wynik.push_back(x);
			dostepne.erase(x.lewo);
			dostepne.erase(x.prawo);
		}
	}
	
	return wynik;
}
/*
Algorytm Prima � algorytm zach�anny wyznaczaj�cy tzw. minimalne drzewo rozpinaj�ce (MDR). 
Maj�c do dyspozycji graf nieskierowany i sp�jny, tzn. taki w kt�rym kraw�dzie grafu nie maj� ustalonego
kierunku oraz dla ka�dych dw�ch wierzcho�k�w grafu istnieje droga pomi�dzy nimi, algorytm oblicza podzbi�r 
E' zbioru kraw�dzi E, dla kt�rego graf nadal pozostaje sp�jny, ale suma koszt�w wszystkich kraw�dzi zbioru E' 
jest najmniejsza mo�liwa[2].
*/
vector<Krawedz> Prim(vector<Krawedz> krawedzie, set<int> wierzcholki) {
	vector<Krawedz> wynik;
	srand(time(NULL));
	vector<int> polaczoneWierzcholki;
	polaczoneWierzcholki.push_back(rand() % wierzcholki.size() + 1);
	wierzcholki.erase(polaczoneWierzcholki.back());
	while (wierzcholki.size() > 0) {
		int najnizszaWaga = 10000;
		int najnizszaWagaID = -5;
		for (int i = 0; i < krawedzie.size(); i++) {
			for (int x : polaczoneWierzcholki) {
				if (((krawedzie[i].lewo == x && wierzcholki.find(krawedzie[i].prawo) != wierzcholki.end()) ||
					(krawedzie[i].prawo == x && wierzcholki.find(krawedzie[i].lewo ) != wierzcholki.end())) &&
					(krawedzie[i].waga < najnizszaWaga || najnizszaWagaID == -5)) {
					najnizszaWaga = krawedzie[i].waga;
					najnizszaWagaID = i;
				}
			}
		}
		wynik.push_back(krawedzie[najnizszaWagaID]);
		wierzcholki.erase(krawedzie[najnizszaWagaID].prawo);
		wierzcholki.erase(krawedzie[najnizszaWagaID].lewo);
		if (find(polaczoneWierzcholki.begin(), polaczoneWierzcholki.end(), krawedzie[najnizszaWagaID].lewo) == polaczoneWierzcholki.end()) {
			polaczoneWierzcholki.push_back(krawedzie[najnizszaWagaID].lewo);
		}
		if (find(polaczoneWierzcholki.begin(), polaczoneWierzcholki.end(), krawedzie[najnizszaWagaID].prawo) == polaczoneWierzcholki.end()) {
			polaczoneWierzcholki.push_back(krawedzie[najnizszaWagaID].prawo);
		}
		vector<Krawedz>::iterator znajdz = find(krawedzie.begin(), krawedzie.end(), krawedzie[najnizszaWagaID]);
		if (znajdz != krawedzie.end()) {
			krawedzie.erase(znajdz);
		}
	}
	return wynik;
}
vector<Krawedz> Boruvka(vector<Krawedz> krawedzie, set<int> wierzcholki) {
	vector<Krawedz> wynik;
	for (auto x : wierzcholki) {
		int najnizszaWaga = -5;
		int najnizszaWagaID = 0;
		for (int i = 0; i < krawedzie.size(); i++) {
			if ((krawedzie[i].lewo == x || krawedzie[i].prawo == x) && 
				(krawedzie[i].waga < krawedzie[najnizszaWagaID].waga || najnizszaWaga == -5)) {
				//cout << "x: " << x << " k.l: " << krawedzie[i].lewo << " k.p: " << krawedzie[i].prawo << " waga: " << krawedzie[i].waga << endl;
				najnizszaWagaID = i;
				najnizszaWaga = krawedzie[i].waga;
			}
		}
		if (find(wynik.begin(), wynik.end(), krawedzie[najnizszaWagaID]) == wynik.end()) {
			wynik.push_back(krawedzie[najnizszaWagaID]);
		}
	}
	for (auto x : wynik) {
		krawedzie.erase(find(krawedzie.begin(), krawedzie.end(), x));
	}
	set<int> polaczone;
	set<int> sprawdzone;
	int id = 1;
	while (polaczone.size() < wierzcholki.size()) {
		for (auto x : wynik) {
			if (x.lewo == id || x.prawo == id) {
				polaczone.insert(x.prawo);
				polaczone.insert(x.lewo);
			}
		}
		int oldId = id;
		sprawdzone.insert(id);
		for (auto x : polaczone) {
			for (auto y : sprawdzone) {
				if (x != y) {
					id = x;
				}
			}
		}
		if (oldId == id) {
			int najnizszaWaga = -5;
			int najnizszaWagaID = 0;
			for (auto x : polaczone) {
				for (int i = 0; i < krawedzie.size(); i++) {
					if ((krawedzie[i].lewo == x && polaczone.find(krawedzie[i].prawo) == polaczone.end()) ||
						(krawedzie[i].prawo == x && polaczone.find(krawedzie[i].lewo) == polaczone.end()) &&
						(krawedzie[i].waga < krawedzie[najnizszaWagaID].waga || najnizszaWaga == -5)) {
						najnizszaWagaID = i;
						najnizszaWaga = krawedzie[i].waga;
					}
				}
			}
			if (find(wynik.begin(), wynik.end(), krawedzie[najnizszaWagaID]) == wynik.end() &&
				(polaczone.find(krawedzie[najnizszaWagaID].prawo) == polaczone.end() ||
				polaczone.find(krawedzie[najnizszaWagaID].lewo) == polaczone.end())) {
				wynik.push_back(krawedzie[najnizszaWagaID]);
			}
			krawedzie.erase(find(krawedzie.begin(), krawedzie.end(), krawedzie[najnizszaWagaID]));
		}
	}
	return wynik;
}

int main() {
	vector<Krawedz> krawedzie; //wczytane z pliku
	set<int> wierzcholki; //dostepne
	vector<Krawedz> Dzewo;
	int suma = 0;
	WczytajPlik("input.txt", krawedzie, wierzcholki);
	clock_t c_start; 
	clock_t c_end;

	c_start = clock();
	Dzewo = Prim(krawedzie, wierzcholki);
	c_end = clock();
	suma = ObliczSumeDrzewa(Dzewo);
	cout << "Czas potrzebny na zbudowanie dzewa: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms" << endl;
	cout << "Wierzcholki ktore buduja drzewo\n(lewy wierzcholek, prawy wierzcholek, waga):\n";
	for (auto x : Dzewo) {
		cout << x.lewo << ", " << x.prawo << ", " << x.waga << '\n';
	}
	cout << "koszt drzewa welde algorytmu prima: " << suma << '\n';

	c_start = clock();
	Dzewo = Boruvka(krawedzie, wierzcholki);
	c_end = clock();
	suma = ObliczSumeDrzewa(Dzewo);
	cout << "Czas potrzebny na zbudowanie dzewa: " << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms" << endl;
	cout << "Wierzcholki ktore buduja drzewo\n(lewy wierzcholek, prawy wierzcholek, waga):\n";
	for (auto x : Dzewo) {
		cout << x.lewo << ", " << x.prawo << ", " << x.waga << '\n';
	}
	cout << "koszt drzewa welde algorytmu boruvka: " << suma << '\n';
	
	system("pause");
	return 0;
}

/*4 5 3
2 5 5
1 2 1
1 3 2
1 4 2
1 5 6
2 4 4
3 4 3*/