#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <cmath>
#include <ctime>
#include <iterator>
#include <algorithm>

using std::cout;
using std::cin;
void writeBits(std::vector<bool> bits) {
	int i = 1;
	for (auto x : bits) {
		cout << x;
		if (i == 8) {
			cout << " ";
			i = 1;
		}
		else i++;
	}
}
std::vector<bool> toBit(char character){
	std::vector<int> temp{ 0,0,0,0,0,0,0,0 };
	int getCharacterASCII = (int)character;

	for (int i = 7; i >= 0; i--) {
		temp[i] = getCharacterASCII % 2;
		getCharacterASCII /= 2;
	}

	std::vector<bool> bits;
	for (auto x : temp) {
		if (x == 0) bits.push_back(false);
		else bits.push_back(true);
	}

	return bits;
}
std::vector<bool> Read_file(std::string file_name, std::string flag, int crcSize){ // 0 - no parity bit, 1 - parity bit
	std::ifstream input(file_name, std::ios::binary);
	std::vector<char> buffer((
		std::istreambuf_iterator<char>(input)),
		(std::istreambuf_iterator<char>()));
	
	std::vector<bool> bits;
	for (auto x : buffer)
	{
		std::vector<bool> temp = toBit(x);
		for (auto B : temp) {
			bits.push_back(B);
		}
	}

	if (flag == "crc") {
		int x;
		std::vector<bool> temp;
		for (int i = 8; i <= 64; i += 8) {
			if ((i - crcSize) >= 0) {
				x = i;
				break;
			}
		}
		for (int i = 0; i < x; i++) {
			if (i < crcSize) {
				temp.push_back(bits.back());
			}
			bits.pop_back();
		}
		for (auto x : temp) {
			bits.push_back(x);
		}
	}

	if (flag == "parity bit") {
		bool temp = bits.back();
		for (int i = 0; i < 8; i++) {
			bits.pop_back();
		}
		bits.push_back(temp);
	}
	return bits;
}
void Write_file(std::string file_name, std::vector<bool> bits, std::string flag, int crcSize)
{
	std::fstream file;
	file.open(file_name, std::ios::out);
	if (!file.good()) {
		std::cout << "This file is corrupted or does not exist!" << std::endl;
	}
	else {
		if (flag == "parity bit"){
			bool temp = bits.back();
			bits.pop_back();
			for (int i = 1; i < 8; i++) {
				bits.push_back(0);
			}
			bits.push_back(temp);
		}
		else if (flag ==  "crc") {
			std::vector<bool> temp; //usu� klucz crc z ci�gu bit�w
			for (int i = 0; i < crcSize; i++) {
				temp.push_back(bits.back());
				bits.pop_back();
			}

			int x; //obliczamy ilo�� brakuj�cych zer (bajty musz� si� zgadza�)
			for (int i = 8; i <= 64; i += 8) {
				if ((i - crcSize) >= 0) {
					x = i;
					break;
				}
			}

			for (int i = 0; i < (x - temp.size()); i++) { //dopisz brakuj�ce zera by klucz crc zape�ni� bajty
				bits.push_back(0);
			}

			for (auto i : temp) { //dopisz klucz crc do pliku;
				bits.push_back(i);
			}


		}
		int i = 0;
		int character = 0;
		for (auto x : bits) {
			if(x) character += pow(2, 7-i);
			if (i == 7) {
				file << char(character);
				i = 0;
				character = 0;
			}
			else i++;
		}
	}
}
void ParityBit(std::vector<bool> &bits) {
	int counter = 0;
	for (auto x : bits) if (x) counter++;
	bits.push_back(counter % 2);
}
std::vector<bool> CreateCrc(std::vector<bool> bits, std::vector<bool> crc_key) {
	std::vector<bool> temp;

	for (int i = 0; i < crc_key.size()-1; i++) {
		bits.push_back(0);
	}

	for (int i = 0; i < (bits.size() - (crc_key.size() - 1)); i++) {
		for (int j = 0; j < crc_key.size(); j++) {
			if (!bits[i]) {
				break;
			}
			else if (bits[i + j] && crc_key[j]) {
				bits[i + j] = false;
			}
			else if (!bits[i + j] && !crc_key[j]) {
				bits[i + j] = false;
			}
			else {
				bits[i + j] = true;
			}
		}
	}
	
	for (int i = crc_key.size()-1; i > 0; i--) {
		temp.push_back(bits[bits.size() - i]);
	}
	cout << "po przeprowadzniu XOR: \n";
	writeBits(bits);
	cout << "\n";
	return temp;
}
std::vector<bool> CreateCrcKey(int size) {
	std::vector<bool> temp{ 1 };
	srand(time(NULL));
	for (int i = 1; i < size; i++) {
		int x = rand() % 2;
		if (x == 1) temp.push_back(true);
		else temp.push_back(false);
	}
	return temp;
}
void disortion(std::vector<bool> &bits, double procentage, bool flag) { // true - bez powt�rze�, false - z powtorzen;{
	int loop = bits.size() * procentage;
	//std::cout << "ilosc bitow do zmiany: " << loop << std::endl;
	srand(time(NULL));
	int temp;

	std::vector<int> repeats;
	if (flag) {
		for (int i = 0; i < bits.size(); i++) {
			repeats.push_back(i);
		}
		std::random_shuffle(repeats.begin(), repeats.end());
	}

	for (int i = 0; i < loop; i++) { //tyle razy ile bit�w mamy zmieni�
		if (flag) { //bez powt�rze�
			temp = repeats[i];
		}
		else {
			temp = rand() % bits.size(); //z powt�rzeniami
		}
		//std::cout << "zmieniono " << (int)bits[temp] << " w miejscu " << temp << std::endl; //do testowania

		if (bits[temp]) bits[temp] = false;
		else bits[temp] = true;
	}

}
std::vector<bool> suma_modulo(std::vector<bool> bits, int modulo) {
	int i = 0;
	int suma = 0;
	int character = 0;
	for (auto x : bits) {
		if (x) character += pow(2, 7 - i);
		if (i == 7) {
			suma += character;
			suma %= modulo;
			i = 0;
			character = 0;
		}
		else i++;
	}

	std::vector<bool> to_bit;
	std::vector<bool> reverse;

	while (suma > 0) {
		to_bit.push_back(suma % 2);
		suma /= 2;
	}
	for (int i = to_bit.size() - 1; i >= 0; i--) {
		reverse.push_back(to_bit[i]);
	}

	
	return reverse;
}
void parityBit_test(double procentage, bool repeats){
	std::vector<bool> bits;
	//wczytujemy bity
	bits = Read_file("dane.txt", "", 0); 
	//dodajemy bit parzystosci
	ParityBit(bits);
	bool remember_old_parity_bit = bits.back();
	cout << "Plik wejscia (z bitem parzystosci): ";
	writeBits(bits);
	cout << "\n\n";
	//zak��� plik i wykonaj przesy�anie i obi�r
	Write_file("parity_bit_1.txt", bits, "parity bit", 0); //zapis z bitem parzystosci
	bits = Read_file("parity_bit_1.txt", "parity bit", 0); //odczyt z bitem parzystosci
	disortion(bits, procentage, repeats); // ostatnia cyfra - 1 bez powt�rze�, 0 z powt�rzeniami
	//podsumowanie
	bool czy_zostal_zaklocony = bits.back();
	if (czy_zostal_zaklocony == remember_old_parity_bit) {
		cout << "Bit parzystosci nie zostal zaklocony (lub byl zaklocany parzysta ilosc razy jesli losowanie z powtorzeniami)\n";
	}
	else {
		cout << "Bit parzystosci zostal zaklocony (ale i tak licze go na nowo XD\n)";
	}
	bits.pop_back();
	ParityBit(bits);
	cout << "Po zakloceniach i odbiorze: ";
	writeBits(bits);
	cout << "\n";
	if (bits.back() == remember_old_parity_bit) {
		cout << "Bit parzystosci jest zgodny\n";
	}
	else {
		cout << "Bit parzystosci jest niezgodny\n";
	}
	Write_file("parity_bit_2.txt", bits, "parity bit", 0); //zapis z bitem parzystosci
	cout << "\n";
}
void moduloSum_test(int modulo, double procentage, bool repeats) {
	std::vector<bool> bits;
	//wczytujemy plik, tworzymy sume modulo
	bits = Read_file("dane.txt", "", 0);
	std::vector<bool> modulo_sum = suma_modulo(bits,modulo);
	cout << "wygenerowana suma modulo: ";
	cout << "\n";
	//dopisujemy sume modulo
	for (auto x : modulo_sum) {
		cout << x;
		bits.push_back(x);
	}
	std::vector<bool> old_modulo = modulo_sum;
	cout << "\n";
	//przesy� i zak�ucenia
	Write_file("modulo_1.txt", bits, "crc", modulo_sum.size()); //bo to ta sama zasada zapisu co w crc XD
	bits = Read_file("modulo_1.txt", "crc", modulo_sum.size());
	disortion(bits, procentage, repeats);
	cout << "Bity po zakloceniach: ";
	writeBits(bits);
	cout << "\n";
	//podsumowanie
	std::vector<bool> modulo_after_disortion;
	for (int i = old_modulo.size(); i > 0; i--) {
		modulo_after_disortion.push_back(bits[bits.size() - i]);
	}
	for (int i = 0; i < old_modulo.size(); i++) {
		bits.pop_back();
	}
	std::vector<bool> new_modulo = suma_modulo(bits, modulo);
	cout << "wejsciowa suma modulo \n";
	for (auto x : old_modulo) {
		cout << x;
	}
	cout << "\npo zakloceniach kod nadmiarowy: \n";
	for (auto x : modulo_after_disortion) {
		cout << x;
	}
	cout << "\nsprawdzony po zakluceniach kod nadmiarowy: \n";
	for (auto x : new_modulo) {
		cout << x;
	}
	cout << "\n";
	Write_file("modulo_2.txt", bits, "crc", modulo_sum.size());
}
void crc_test(int crc_length, double procentage, bool repeats) { //do 64 bit�w, stwierdzi�em, �e wi�cej nie ma sensu XD
	std::vector<bool> bits;
	//wczytujemy plik, tworzymy wielomian crc i przeprowadzamy xor
	bits = Read_file("dane.txt", "", 0);
	std::vector<bool> crc_key = CreateCrcKey(crc_length);
	cout << "wygenerowany wielomian crc: ";
	for (auto x : crc_key) {
		cout << x;
	}
	cout << "\n";
	std::vector<bool> crc = CreateCrc(bits, crc_key);
	//kod nadmiarowy
	for (auto x : crc) {
		cout << x;
		bits.push_back(x);
	}
	std::vector<bool> old_crc = crc;
	cout << "\n";
	//zak��cenia i przesy�
	Write_file("crc_1.txt", bits, "crc", crc.size());
	bits = Read_file("crc_1.txt", "crc", crc.size());
	disortion(bits, procentage, repeats);
	cout << "Bity po zakloceniach: ";
	writeBits(bits);
	cout << "\n";
	//podsumowanie
	std::vector<bool> crc_after_disortion;
	for (int i = crc_key.size() - 1; i > 0; i--) {
		crc_after_disortion.push_back(bits[bits.size() - i]);
	}
	for (int i = 0; i < crc_length - 1; i++) {
		bits.pop_back();
	}
	std::vector<bool> new_crc = CreateCrc(bits, crc_key);
	cout << "wejsciowy kod nadmiarowy: \n";
	for (auto x : old_crc) {
		cout << x;
	}
	cout << "\npo zakloceniach kod nadmiarowy: \n";
	for (auto x : crc_after_disortion) {
		cout << x;
	}
	cout << "\nsprawdzony po zakluceniach kod nadmiarowy: \n";
	for (auto x : new_crc) {
		cout << x;
	}
	cout << "\n";
	Write_file("crc_2.txt", bits, "crc", crc.size());
}

int main()
{
	parityBit_test(0.05, false); //5% b��d�w zak�ucenia bez powt�rze�
	cout << "\n\n\n\n";
	moduloSum_test(19,0.05, false);//5% b��d�w a^b mod 8, zak�ucenia bez powt�rze�
	cout << "\n\n\n\n";
	crc_test(8, 0.05, false); //5% b��d�w wielomian crc 16 bitowy, zak�ucenia bez powt�rze�
	system("pause");
	return 0;
}