#include <iostream>
#include <fstream>
#include <cstdint>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/*
przyk�ad pragmy
#pragma pack(push, 1)
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
	int i;
	char a[20];
} StrukturaP1;

typedef struct {
	int i;
	char a[21];
} StrukturaP2;
#pragma pack(pop)
*/

struct Pakiet {
	int ID;
	int size;

	Pakiet(int ID, int size):ID(ID), size(size) {};
};

vector<char> read_file(string fileName) {
	vector<char> odczyt;
	fstream file;
	file.open(fileName, ios::in|ios::binary);
	if (!file.good()) {
		cout << "dupadebugging\n";
	}
	char temp;
	while (file.get(temp)) {
		odczyt.push_back(temp);
	}
	return odczyt;
}

void write_file(string file_name, vector<char*> &data) {
	fstream file;
	file.open(file_name, ios::out|ios::binary);
	if (!file.good()) {
		cout << "This file is corrupted or does not exist!" << endl;
	}
	for (int i = 0; i < data.size(); i++) {
		Pakiet* pakiet = (Pakiet*)data[i];
		char* dane = data[i] + sizeof(Pakiet);
		file << pakiet->ID << " " << pakiet->size << " ";
		file.write(dane, pakiet->size);
		file << ' ';
	}
	file.close();
}

void writeFileBack(string file_name, vector<char*> &data) {
	fstream file;
	file.open(file_name, ios::out | ios::binary);
	if (!file.good()) {
		cout << "This file is corrupted or does not exist!" << endl;
	}
	for (int i = 0; i < data.size(); i++) {
		Pakiet* pakiet = (Pakiet*)data[i];
		char* dane = data[i] + sizeof(Pakiet);
		for (int j = 0; j < pakiet->size; j++) {
			file << *dane;
			dane++;
		}
	}
	file.close();
}

vector<char*> readPacketsFromFile(string fileName) {
	vector<char*> data;
	fstream file;
	file.open(fileName, ios::in | ios::binary);
	if (!file.good()) {
		cout << "This file is corrupted or does not exist!" << endl;
	}
	int tempID;
	int tempSize;
	char* tempData;
	int i = 0;
	while (file >> tempID && file >> tempSize) {
		tempData = new char[tempSize];
		if (file.peek() == ' ') file.ignore(1); //cruszial
		file.read(tempData, tempSize);
		data.push_back(new char[sizeof(Pakiet) + tempSize * sizeof(char)]);
		Pakiet* pakiet = (Pakiet*)data.back();
		pakiet->ID = tempID;
		pakiet->size = tempSize;
		char* dane = (char*)(data.back() + sizeof(Pakiet));
		for (int j = 0; j < tempSize; j++) {
			*dane = tempData[j];
			dane++;
		}
		//cout << file.peek() << endl;
		/*printf("ID pakietu: %i, rozmiar pakietu: %i i jego tresc:", pakiet->ID, pakiet->size);
		cout.write((char*)(data.back() + sizeof(Pakiet)), pakiet->size);
		cout << endl;*/
	}
	file.close();
	return data;
}

int main(int argc, char *argv[])
{
	int maksRozmiarPakietu = 8;
	vector<char> tekst = read_file("XD.jpg");
	vector<char*> daneJakoPakiety;
	int indeks = 0;
	//cout << "Dane po odczycie z pliku dane.txt: \n";
	for (int i = 0; i < tekst.size(); i += maksRozmiarPakietu) {
		int rozmiar = 0;
		if (i + maksRozmiarPakietu <= tekst.size() - 1) {
			rozmiar =  maksRozmiarPakietu;
		}
		else {
			rozmiar = (tekst.size()) % i;
		}
			daneJakoPakiety.push_back(new char[sizeof(Pakiet) + rozmiar*sizeof(char)]);
			Pakiet* pakiet = (Pakiet*)daneJakoPakiety.back();
			pakiet->ID = indeks;
			indeks++;
			pakiet->size = rozmiar;
			char* dane = daneJakoPakiety.back() + sizeof(Pakiet);
			for (int j = i; j < i + rozmiar; j++) {
				*dane = tekst[j];
				dane++;
			}
			//printf("ID pakietu: %i, rozmiar pakietu: %i i jego tresc:", pakiet->ID, pakiet->size);
			//cout.write((char*)(daneJakoPakiety.back() + sizeof(Pakiet)), pakiet->size);
			//cout << endl;
	}
	random_shuffle(daneJakoPakiety.begin(), daneJakoPakiety.end());
	write_file("wyjscie.txt", daneJakoPakiety);
	for (int i = daneJakoPakiety.size()-1; i >= 0; i--) {
		delete[] daneJakoPakiety[i];
	}
	//cout << "dane po odczycie z pliku wyjscie.txt: \n";
	daneJakoPakiety = readPacketsFromFile("wyjscie.txt");
	for (auto x : daneJakoPakiety) {
		Pakiet* pakiet = (Pakiet*)x;
		//printf("ID pakietu: %i, rozmiar pakietu: %i i jego tresc:", pakiet->ID, pakiet->size);
		//cout.write((char*)(x + sizeof(Pakiet)), pakiet->size);
		//cout << endl;
	}

	vector<char*> ulozone;
	//cout << "tekst po ulozeniu: \n";
	for (int i = 0; i < daneJakoPakiety.size(); i++) {
		for (auto x : daneJakoPakiety) {
			Pakiet* pakiet = (Pakiet*)x;
			if (pakiet->ID == i) {
				ulozone.push_back(x);
				//cout.write((char*)(x + sizeof(Pakiet)), pakiet->size);
				break;
			}
		}
	}
	writeFileBack("XDD.jpg", ulozone);
	for (int i = daneJakoPakiety.size() - 1; i >= 0; i--) {
		delete[] daneJakoPakiety[i];
	}
	cout << endl;
	system("PAUSE");
	return EXIT_SUCCESS;
}