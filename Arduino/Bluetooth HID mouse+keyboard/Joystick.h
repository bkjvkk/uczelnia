#pragma once
#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <Arduino.h>
#include "Settings.h"

struct Joystick
{
public:
	Joystick();
	void hardwareInit();
	void toggleMode();
	bool canMove();
  bool canScroll();
	bool getMode();
	int getX();
	int getY();
private:
	unsigned long movementPause;
	unsigned long modeSwitchPause;
	bool mode; //0 = osie x i y, 1 tylko o�
};

#endif
