//WYMAGA <SOFTWARESERIAL.H>

#pragma once
#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Settings.h"
#include "Joystick.h"
#include "NewKeyboard.h"

class Bluetooth
{
public:
	Bluetooth(uint32_t baudRate);
	~Bluetooth();

	void operateJoystick(Joystick *joystick);
	void operateKeyboard(NewKeyboard *keyboard);

private:
	Stream * stream;
    void joystickXY(Joystick *joystick);
    void joystickZ(Joystick *joystick);

	void keyboardMouse(NewKeyboard *keyboard);
	void keyboardVolume(NewKeyboard *keyboard);
};

#endif