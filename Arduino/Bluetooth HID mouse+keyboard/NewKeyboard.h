#pragma once
#ifndef NEWKEYBOARD_H
#define NEWKEYBOARD_H

#include <Arduino.h>
#include "Settings.h"

struct NewKeyboard
{
public:
	NewKeyboard();
	void hardwareInit(); //INPUT_PULLUP stan domyślny na pinach 1
    void toggleMode(); 
    bool getMode();
    //sprawdzamy stan wciśniecia przycisku
    void unlockMode();
    //stany: 1 - wcisniety / 2 - byl wcisniety, juz nie jest / 3 - nie byl
    bool isPressed(int button);
    void toggleButton(int button, bool keystroke);
    unsigned long timeDiff(int button);
    int state(int button);
    int useLeftButton();
//private:
    //blokada zmiany trybów
    bool leftPressed;
    bool rightPressed;
    bool modePressed;
    //ostatnie czasy uzycia przyciskow
    unsigned long leftTime;
    unsigned long rightTime;
    unsigned long modeTime;
    bool mode; //0 - lpm i ppm 1 - volume up i down
};

#endif
