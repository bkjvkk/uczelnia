//WYMAGA <SERIALSTREAM.H>
#include "Bluetooth.h"

Bluetooth::Bluetooth(uint32_t baudRate)
{
	stream = new SoftwareSerial(NEWRX, NEWTX);
	((SoftwareSerial*)stream)->begin(baudRate);
}

Bluetooth::~Bluetooth()
{
		delete(stream);
		stream = 0;
}

void Bluetooth::operateJoystick(Joystick *joystick)
{
    //Jesli nacisnieto przycisk zmien tryb joysticka
    joystick->toggleMode();
    //Jesli 1
    if(joystick->getMode())
    {
        this->joystickZ(joystick);
    } //Jesli 0
    else
    {
        this->joystickXY(joystick);
    }
    
}

void Bluetooth::joystickXY(Joystick *joystick)
{
    if(joystick->canMove())
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)joystick->getX());
    stream->write((uint8_t)joystick->getY());
    stream->write((uint8_t)0x00);
  }
}

void Bluetooth::joystickZ(Joystick *joystick)
{
    if(joystick->canScroll())
  {
      auto y = joystick->getY();
      if(y < 0)
      {
          y /= (y*-1);
      }
      else if(y != 0)
      {
          y /= y;
      }


    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)-y);
  }
}

void Bluetooth::operateKeyboard(NewKeyboard *keyboard)
{
  //Jesli nacisnieto przycisk zmien tryb joysticka
    keyboard->toggleMode();
    //Jesli 1
    if(keyboard->getMode())
    {
        this->keyboardVolume(keyboard);
    } //Jesli 0
    else
    {
        this->keyboardMouse(keyboard);
    }
}

void Bluetooth::keyboardMouse(NewKeyboard *keyboard)
{
  int state = keyboard->state(LEFTBUTTON); //left button
  if(state == KEYSTROKE) // wyślij sygnal wcisnietego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x01);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
  else if(state == RELEASE) //wyslij sygnal puszczonego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
  
  state = keyboard->state(RIGHTBUTTON); //right button

  if(state == KEYSTROKE) // wyślij sygnal wcisnietego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
  else if(state == RELEASE) //wyslij sygnal puszczonego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x05);
    stream->write((uint8_t)0x02);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
}

void Bluetooth::keyboardVolume(NewKeyboard *keyboard)
{
  int state = keyboard->state(LEFTBUTTON);

  if(state == KEYSTROKE) // wyślij sygnal wcisnietego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x10);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
  else if(state == RELEASE) //wyslij sygnal puszczonego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }

  state = keyboard->state(RIGHTBUTTON); //right button

  if(state == KEYSTROKE) // wyślij sygnal wcisnietego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x20);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
  else if(state == RELEASE) //wyslij sygnal puszczonego przycisku
  {
    stream->write((uint8_t)0xFD);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x03);
    stream->write((uint8_t)0x00);
    stream->write((uint8_t)0x00);
  }
}
