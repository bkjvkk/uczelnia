#include "NewKeyboard.h"

NewKeyboard::NewKeyboard():leftPressed(false), rightPressed(false),modePressed(false),leftTime(millis()),rightTime(millis()),modeTime(millis()), mode(0)
{
    
}

void NewKeyboard::hardwareInit()
{
    pinMode(LEFTBUTTON, INPUT_PULLUP);
    pinMode(RIGHTBUTTON, INPUT_PULLUP);
    pinMode(MODEBUTTON, INPUT_PULLUP);
}

void NewKeyboard::toggleMode()
{
    if (digitalRead(MODEBUTTON) == 0 && millis() - modeTime > BUTTONPAUSE && !modePressed)
	{
        //zmien tryb pracy
		mode = !mode;
        //ustaw czas ostatniego wcisniecia
		modeTime = millis();
        //zablokuj przycisk
        modePressed = true;
	}
    else
    {
        unlockMode();
    }
    
}

bool NewKeyboard::getMode()
{
    return mode;
}

void NewKeyboard::unlockMode()
{
    if(digitalRead(MODEBUTTON) == 1 && modePressed)
    {
        //Wylacz blokade przycisku trybow
        modePressed = false;
    }
}
bool NewKeyboard::isPressed(int button)
{
    if(button == LEFTBUTTON)
    {
        return leftPressed;
    }
    else if(button == RIGHTBUTTON)
    {
        return rightPressed;
    }
}


void NewKeyboard::toggleButton(int button, bool toggle)
{
    if(button == LEFTBUTTON)
    {
        if(toggle)
        {
          leftPressed = !leftPressed;
        }
        leftTime = millis();
    }
    else if(button == RIGHTBUTTON)
    {
        if(toggle)
        {
          rightPressed = !rightPressed;
        }
        rightTime = millis();
    }

}

unsigned long NewKeyboard::timeDiff(int button)
{
    if(button == LEFTBUTTON)
    {
        return millis() - leftTime;
    }
    else if(button == RIGHTBUTTON)
    {
        return millis() - rightTime;
    }
}

int NewKeyboard::state(int button)
{
    if(timeDiff(button) > BUTTONPAUSE)
    {
        int buttonRead = digitalRead(button); 
        if(buttonRead == 1 && !(this->isPressed(button))) //idle, przycisk nie byl i nie jest wcisniety
        {
            return INACTIVE;
        }
        else if(buttonRead == 1 && this->isPressed(button)) //released, przycisk nie jest trzymany, ale byl
        {
            this->toggleButton(button,true);
            return RELEASE;
        }
        else if(buttonRead == 0 && !(this->isPressed(button))) //keystroke, jest nacisniety, a nie byl
        {
            this->toggleButton(button, true);
            return KEYSTROKE;
        }
        else //hold, jest i byl nacisniety
        {
            return HOLD;
        }
    }
    else
    {
        return AWAIT;
    }
}
