#include "Joystick.h"

Joystick::Joystick():mode(0), movementPause(millis()), modeSwitchPause(millis())
{

}

void Joystick::hardwareInit() {

	//Przycisk joysticka
	pinMode(SW, INPUT);
	digitalWrite(SW, HIGH);
	//Osie X / Y
	pinMode(VRX, INPUT);
	pinMode(VRY, INPUT);

}

void Joystick::toggleMode()
{
	if (digitalRead(SW) == 0 && millis() - modeSwitchPause > TOGGLEMODE)
	{
		mode = !mode;
		modeSwitchPause = millis();
	}
}

bool Joystick::getMode()
{
	return mode;
}

int Joystick::getX()
{
	int x = analogRead(VRX);
	if (x > (X_AV + TH) || x < (X_AV - TH))
	{
		x = (x - X_AV) / DIVIDER;
	}
	else
	{
		x = 0;
	}
	return x;
}

int Joystick::getY()
{
	int y = analogRead(VRY);
	if (y > (Y_AV + TH) || y < (Y_AV - TH))
	{
		y = (y - Y_AV) / DIVIDER;
	}
	else
	{
		y = 0;
	}
	return y;
}

bool Joystick::canMove()
{
	if((this->getX() != 0 || this->getY() != 0) && (millis() - movementPause > MOVEMENTPAUSE))
	{
		movementPause = millis();
		return true;
	}
	else
	{
		return false;
	}
	
}

bool Joystick::canScroll()
{
  if((this->getY() != 0) && (millis() - movementPause > SCROLLPAUSE))
  {
    movementPause = millis();
    return true;
  }
  else
  {
    return false;
  }
  
}
