#include <SoftwareSerial.h>
#include "Joystick.h"
#include "NewKeyboard.h"
#include "Bluetooth.h"

Joystick joystick;
NewKeyboard keyboard;
void setup()  
{  
  pinMode(NEWRX, INPUT);
  pinMode(NEWTX, OUTPUT);
  Serial.begin(115200);
  joystick.hardwareInit();
  keyboard.hardwareInit();
}  

Bluetooth bluetooth(115200);

void loop()  
{  
  Serial.print(keyboard.timeDiff(LEFTBUTTON));;
  Serial.print(" : ");
  Serial.print(keyboard.state(LEFTBUTTON));
  Serial.print(" ");
  Serial.print(keyboard.leftPressed);
  Serial.println();

  Serial.println("");
  bluetooth.operateJoystick(&joystick);
  bluetooth.operateKeyboard(&keyboard);
}  
