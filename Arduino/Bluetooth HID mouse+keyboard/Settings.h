#pragma once
#ifndef SETTINGS_H
#define SETTINGS_H

//----------------JOYSTICK----------------
//Ustawienia pin�w joysticka
#define VRY A5
#define VRX A4
#define SW  A3
//�rodkowe warto�ci joysticka
#define X_AV 490
#define Y_AV 525
//Pr�g reagowania (threshold)
#define TH 5
//Ogranicznie sygna�u
#define DIVIDER 32
//Pauza do zmiany mod�w
#define TOGGLEMODE 250
//Pauza do poruszania
#define MOVEMENTPAUSE 15
#define SCROLLPAUSE 100

//----------------BLUETOOTH----------------
//Ustawienia przesyłania SoftwareSerial
//Do tych portów podlaczymy Rx i Tx modulu bluetooth
#define NEWRX 10
#define NEWTX 11

//----------------KEYBOARD----------------
//Ustawienia pinow
#define LEFTBUTTON 2
#define RIGHTBUTTON 3
#define MODEBUTTON 4
//Ustawienia czasow oczekiwania miedzy sprawdzeniem stanow przyciskow
#define BUTTONPAUSE 40
//status przyciskow
#define INACTIVE 0
#define RELEASE 1
#define KEYSTROKE 2
#define HOLD 3
#define AWAIT 4
#define TEST 5

#endif
