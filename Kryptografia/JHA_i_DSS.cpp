#include "helper_functions.hpp"

int n1(string m) //licz samog�oski
{
	int iterator = 0;
	for (int i = 0; i < m.size();i++)
	{
		switch (m[i])
		{
			case 'a': case 'e': case 'i':case 'o':case 'u':case 'A': case 'E': case 'I':case 'O':case 'U':
				iterator++;
			break;
		default:
			continue;
			break;
		}
	}
	return iterator;
} 
int n2(string m) // licz sp�g�oski
{
	int iterator = 0;
	for (int i = 0; i < m.size();i++)
	{
		switch (m[i])
		{
		case 'b': case 'c': case 'd':case 'f':case 'g':case 'B': case 'C': case 'D':case 'F':case 'G':
		case 'h': case 'j': case 'k':case 'l':case 'm':case 'H': case 'J': case 'K':case 'L':case 'M':
		case 'n': case 'p': case 'q':case 'r':case 's':case 'N': case 'P': case 'Q':case 'R':case 'S':
		case 't': case 'w': case 'x':case 'y':case 'z':case 'T': case 'W': case 'X':case 'Y':case 'Z':
			iterator++;
			break;
		default:
			continue;
			break;
		}
	}
	return iterator;
} 
int SP(string m) //licz spacje
{
	int iterator = 0;
	for (int i = 0; i < m.size();i++)
	{
		switch (m[i])
		{
		case ' ': 
			iterator++;
			break;
		default:
			continue;
			break;
		}
	}
	return iterator;
}
int JHA(string m, int p, int q)
{
	int samogloski = n1(m);
	int spolgloski = n2(m);
	int spacje = SP(m);
	//cout << 7 * samogloski << " " << 3 * spolgloski << " " << pow(spacje, 2) << " " << endl;
	int test = 7 * samogloski - 3 * spolgloski + pow(spacje, 2);
	
	if(test > 0) return s_p_m(q, test, p);
	else if (test < 0) return s_p_m(odwrocone_mod(q, p),fabs(test),p);
	else return 1;
}
bool input_check(int p,int g, int q, int k,int r)
{
	if (s_p_m(g, q, p) != 1) return false;
	else if (k >= q || k < 0) return false;
	else if (r >= q || r < 0) return false;
	else return true;
}
int create_q(int p)
{
	//Tworzymy klas� wektora licz�cego kt�ry zbiera wyniki w algorytmie fermata
	//na wszelki wypadek sortujemy wyniki maj�c pewno��, �e najwi�kszy element b�dzie ostatnim elementem
	Counting_vec q_calculate;
	fermata(p, 0, q_calculate);
	q_calculate.sort();
	return q_calculate.get(q_calculate.num_of_elements() - 1);
}


int main()
{
	int p,g,k,r,q,x,y;
	fstream odczyt;
	string m = "";
	char letter;

	cout << "podaj kolejno p,g,k,r: ";
	cin >> p >> g >> k >> r;

	if (if_primary(p)) q = create_q(p);
	else { cout << "p nie jest liczba pierwsza\n"; return -3; }

	
	if(!input_check(p,g,q,k,r)) return -2;

	cout << "klucz podstawowy: (" << s_p_m(g, k, p) << ", " << g << ", " << p << ", " << q << ")\n";

	
	odczyt.open("dane.txt", ios::in);
	if (odczyt.good() != true) { cout << "plik dane.txt nie dziala\n"; return -1; } //jak plik nie dzia�a, to zako�cz program

	//czytamy tekst z pliku i zapisujemy go do zmiennej
	while (odczyt.get(letter)) m += letter;
	
	cout << "JHA: " << JHA(m, p, q) << endl;

	x = s_p_m(g, k, p) % q;
	y = (odwrocone_mod(r, q)*((JHA(m, p, q) + k * x) % q)) % q;
	cout << "klucz publczny: (" << x << ", " << y << ")\n";
	//for (int i = 0; i < m.size();i++) cout << m[i];
	cout << endl;
	odczyt.close();

	system("Pause");
	return 0;
}

/*
return type errors:
-1 - plik wej�ciowy txt si� nie otworzy�
-2 - wartosc g lub k moga byc bledne
-3 - p nie jest liczb� pierwsz�

*/