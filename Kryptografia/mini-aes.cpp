#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

struct matrix {
	vector<int> c11;
	vector<int> c12;
	vector<int> c21;
	vector<int> c22;
	matrix(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14, int a15, int a16)
	{
		c11.push_back(a1);
		c11.push_back(a2);
		c11.push_back(a3);
		c11.push_back(a4);

		c12.push_back(a5);
		c12.push_back(a6);
		c12.push_back(a7);
		c12.push_back(a8);

		c21.push_back(a9);
		c21.push_back(a10);
		c21.push_back(a11);
		c21.push_back(a12);

		c22.push_back(a13);
		c22.push_back(a14);
		c22.push_back(a15);
		c22.push_back(a16);
	}
	matrix(matrix &a)
	{
		c11.push_back(a.c11[0]);
		c11.push_back(a.c11[1]);
		c11.push_back(a.c11[2]);
		c11.push_back(a.c11[3]);

		c12.push_back(a.c12[0]);
		c12.push_back(a.c12[1]);
		c12.push_back(a.c12[2]);
		c12.push_back(a.c12[3]);

		c21.push_back(a.c21[0]);
		c21.push_back(a.c21[1]);
		c21.push_back(a.c21[2]);
		c21.push_back(a.c21[3]);

		c22.push_back(a.c22[0]);
		c22.push_back(a.c22[1]);
		c22.push_back(a.c22[2]);
		c22.push_back(a.c22[3]);
	}
};
void print_matrix(const matrix a)
{
	cout << "{" << a.c11[0] << ", " << a.c11[1] << ", " << a.c11[2] << ", " << a.c11[3] << "} ";
	cout << "{" << a.c12[0] << ", " << a.c12[1] << ", " << a.c12[2] << ", " << a.c12[3] << "}\n";
	cout << "{" << a.c21[0] << ", " << a.c21[1] << ", " << a.c21[2] << ", " << a.c21[3] << "} ";
	cout << "{" << a.c22[0] << ", " << a.c22[1] << ", " << a.c22[2] << ", " << a.c22[3] << "}\n";
}
void sprawdz_przesuniecie(int &przesuniecie)
{
	switch (przesuniecie)
	{
	case 0: case 1:
		przesuniecie = 0;
		break;
	case 2: case 3:
		przesuniecie = 1;
		break;
	case 4: case 5:
		przesuniecie = 2;
		break;
	case 6: case 7:
		przesuniecie = 3;
	default:
		cout << "error w analizie przesuniecia\n";
		break;
	}
}
void swap_vec(vector<int> &a)
{
	vector<int> toswap;
	for (int i = a.size() - 1;i >= 0;i--) toswap.push_back(a[i]);
	a = toswap;
}
vector<int> dec_to_bin(int a)
{
	vector<int> wynik;
	while (a > 0)
	{
		wynik.push_back(a % 2);
		a /= 2;
	}
	for (int i = wynik.size(); i < 4;i++) wynik.push_back(0);
	swap_vec(wynik);
	return wynik;
}
int bin_to_dec(vector<int> a)
{
	int liczba = 0;
	swap_vec(a);
	for (int i = 0;i < a.size();i++) if (a[i] == 1) liczba += pow(2, i);
	return liczba;
}
vector<int> dzielenie2(vector<int> a)
{
	int calc = 19;
	int liczba = bin_to_dec(a);
	//cout << liczba << endl;
	calc = liczba % 19;
	return dec_to_bin(calc);
}
int xor(int a, int b)
{
	if (a == b) return 0;
	else return 1;
}
void reduction(vector<int> &a)
{
	swap_vec(a);
	while (a[a.size() - 1] == 0 && a.size() > 4) a.pop_back();
	swap_vec(a);
}
vector<int> dzielenie(vector<int> a)
{
	const vector<int> mod = { 1,0,0,1,1 };
	reduction(a);

	while (mod.size() <= a.size())
	{
		for (int i = 0;i < 5;i++) a[i] = xor (mod[i], a[i]);
		reduction(a);
	}
	return a;
}
vector<int> mnozenie_zle(vector<int> a, vector<int> b)
{
	int przesuniecie;
	vector<int> pomnozone;
	pomnozone.push_back(a[3] * b[3]); //7

	przesuniecie = a[2] * b[3] + a[3] * b[2]; //6
	pomnozone.push_back(przesuniecie % 2);
	sprawdz_przesuniecie(przesuniecie);
	//cout << przesuniecie << " ";

	przesuniecie = a[1] * b[3] + a[2] * b[2] + a[3] * b[1] + przesuniecie; //5
	pomnozone.push_back(przesuniecie % 2);
	sprawdz_przesuniecie(przesuniecie);
	//cout << przesuniecie << " ";

	przesuniecie = a[0] * b[3] + a[1] * b[2] + a[2] * b[1] + a[3] * b[0] + przesuniecie; //4
	pomnozone.push_back(przesuniecie % 2);
	sprawdz_przesuniecie(przesuniecie);
	//cout << przesuniecie << " ";

	przesuniecie = a[0] * b[2] + a[1] * b[1] + a[2] * b[0] + przesuniecie; //3
	pomnozone.push_back(przesuniecie % 2);
	sprawdz_przesuniecie(przesuniecie);
	//cout << przesuniecie << " ";

	przesuniecie = a[0] * b[1] + a[1] * b[0] + przesuniecie; //2
	pomnozone.push_back(przesuniecie % 2);
	sprawdz_przesuniecie(przesuniecie);
	//cout << przesuniecie << " ";

	przesuniecie = a[0] * b[0] + przesuniecie; //1
	pomnozone.push_back(przesuniecie % 2);
	//cout << przesuniecie<< endl;
	//sprawdz_przesuniecie(przesuniecie);

	switch (przesuniecie)
	{
	case 0: case 1:
		break;
	case 2: case 3:
		pomnozone.push_back(1);
		break;
	case 4: case 5:
		pomnozone.push_back(0);
		pomnozone.push_back(1);
		break;
	default:
		cout << "Cos poszlo nie tak z przesunieciem!\n";
		break;
	}
	vector<int> wynik;
	for (int i = pomnozone.size() - 1; i >= 0;i--) wynik.push_back(pomnozone[i]);

	//for (int i : wynik) cout << i << " "; cout << endl;
	return dzielenie(wynik);
}
vector<int> mnozenie(vector<int> a, vector<int> b)
{
	vector<int> pomnozone;
	pomnozone.push_back(a[3] * b[3]);
	pomnozone.push_back((a[2] * b[3] + a[3] * b[2]) % 2);
	pomnozone.push_back((a[1] * b[3] + a[2] * b[2] + a[3] * b[1]) % 2);
	pomnozone.push_back((a[0] * b[3] + a[1] * b[2] + a[2] * b[1] + a[3] * b[0]) % 2);
	pomnozone.push_back((a[0] * b[2] + a[1] * b[1] + a[2] * b[0]) % 2);
	pomnozone.push_back((a[0] * b[1] + a[1] * b[0]) % 2);
	pomnozone.push_back((a[0] * b[0]) % 2);
	vector<int> wynik;
	for (int i = pomnozone.size() - 1; i >= 0;i--) wynik.push_back(pomnozone[i]);
	//for (int i : wynik) cout << i << " "; cout << endl;
	return dzielenie(wynik);
}
vector<int> dodawanie(vector<int> a, vector<int> b)
{
	vector<int> obliczenia;
	int przesuniecie = 0;
	for (int i = a.size() - 1; i >= 0;i--)
	{
		if (a[i] == b[i]) obliczenia.push_back(0);
		else obliczenia.push_back(1);
	}
	swap_vec(obliczenia);
	return obliczenia;
}
void ZK(matrix &a)
{
	vector<int>temp;
	temp = a.c22;
	a.c22 = a.c21;
	a.c21 = temp;
}
void SBoxD(vector<int> &a)
{
	switch (bin_to_dec(a))
	{
	case 0:
		a[0] = 1; a[1] = 1; a[2] = 1; a[3] = 0;
		break;
	case 1:
		a[0] = 0; a[1] = 0; a[2] = 1; a[3] = 0;
		break;
	case 2:
		a[0] = 0; a[1] = 1; a[2] = 1; a[3] = 1;
		break;
	case 3:
		a[0] = 1; a[1] = 0; a[2] = 0; a[3] = 0;
		break;
	case 4:
		a[0] = 0; a[1] = 0; a[2] = 0; a[3] = 0;
		break;
	case 5:
		a[0] = 1; a[1] = 1; a[2] = 0; a[3] = 1;
		break;
	case 6:
		a[0] = 1; a[1] = 0; a[2] = 1; a[3] = 0;
		break;
	case 7:
		a[0] = 1; a[1] = 1; a[2] = 1; a[3] = 0;
		break;
	case 8:
		a[0] = 0; a[1] = 1; a[2] = 1; a[3] = 1;
		break;
	case 9:
		a[0] = 1; a[1] = 1; a[2] = 0; a[3] = 1;
		break;
	case 10:
		a[0] = 1; a[1] = 0; a[2] = 0; a[3] = 1;
		break;
	case 11:
		a[0] = 0; a[1] = 1; a[2] = 1; a[3] = 0;
		break;
	case 12:
		a[0] = 1; a[1] = 0; a[2] = 1; a[3] = 1;
		break;
	case 13:
		a[0] = 0; a[1] = 0; a[2] = 1; a[3] = 0;
		break;
	case 14:
		a[0] = 0; a[1] = 0; a[2] = 0; a[3] = 0;
		break;
	case 15:
		a[0] = 0; a[1] = 1; a[2] = 0; a[3] = 1;
		break;
	};
}
void SBoxE(vector<int> &a)
{
	switch (bin_to_dec(a))
	{
	case 0:
		a[0] = 1; a[1] = 1; a[2] = 1; a[3] = 0;
		break;
	case 1:
		a[0] = 0; a[1] = 1; a[2] = 0; a[3] = 0;
		break;
	case 2:
		a[0] = 1; a[1] = 1; a[2] = 0; a[3] = 1;
		break;
	case 3:
		a[0] = 0; a[1] = 0; a[2] = 0; a[3] = 1;
		break;
	case 4:
		a[0] = 0; a[1] = 0; a[2] = 1; a[3] = 0;
		break;
	case 5:
		a[0] = 1; a[1] = 1; a[2] = 1; a[3] = 1;
		break;
	case 6:
		a[0] = 1; a[1] = 0; a[2] = 1; a[3] = 1;
		break;
	case 7:
		a[0] = 1; a[1] = 0; a[2] = 0; a[3] = 0;
		break;
	case 8:
		a[0] = 0; a[1] = 0; a[2] = 1; a[3] = 1;
		break;
	case 9:
		a[0] = 1; a[1] = 0; a[2] = 1; a[3] = 0;
		break;
	case 10:
		a[0] = 0; a[1] = 1; a[2] = 1; a[3] = 0;
		break;
	case 11:
		a[0] = 1; a[1] = 1; a[2] = 0; a[3] = 0;
		break;
	case 12:
		a[0] = 0; a[1] = 1; a[2] = 0; a[3] = 1;
		break;
	case 13:
		a[0] = 1; a[1] = 0; a[2] = 0; a[3] = 1;
		break;
	case 14:
		a[0] = 0; a[1] = 0; a[2] = 0; a[3] = 0;
		break;
	case 15:
		a[0] = 0; a[1] = 1; a[2] = 1; a[3] = 1;
		break;
	};
}
void set_k2(matrix &k2, matrix kp)
{
	vector<int> temp;
	temp = kp.c22;
	SBoxE(temp);
	k2.c11 = dodawanie(dodawanie(kp.c11, temp), { 0,0,1,0 });
	k2.c21 = dodawanie(kp.c21, k2.c11);
	k2.c12 = dodawanie(kp.c12, k2.c21);
	k2.c22 = dodawanie(kp.c22, k2.c12);
	cout << "klucz k2:\n"; print_matrix(k2);
}
void set_k1(matrix &k1, matrix kp)
{
	vector<int> temp;
	temp = kp.c22;
	SBoxE(temp);
	k1.c11 = dodawanie(dodawanie(kp.c11, temp), { 0,0,0,1 });
	k1.c21 = dodawanie(kp.c21, k1.c11);
	k1.c12 = dodawanie(kp.c12, k1.c21);
	k1.c22 = dodawanie(kp.c22, k1.c12);
	cout << "klucz k1:\n";print_matrix(k1);
}
void szyfrowanie(matrix &t)
{
	matrix kp{ 1, 0, 1, 1 , 0, 1, 1, 0 ,1, 1, 1, 0 , 0, 1, 1, 0 };
	matrix k1{ 1, 0, 1, 1 , 0, 0, 1, 0 ,1, 1, 1, 1 , 0, 1, 1, 0 };
	set_k1(k1, kp);
	matrix k2{ 1, 0, 1, 1 , 0, 0, 1, 0 ,1, 1, 1, 1 , 0, 1, 1, 0 };
	set_k2(k2, k1);
	//Dodawanie klucza pocz�tkowego kp do tekstu t: t = t + kp.
	t.c11 = dodawanie(t.c11, kp.c11);
	t.c12 = dodawanie(t.c12, kp.c12);
	t.c21 = dodawanie(t.c21, kp.c21);
	t.c22 = dodawanie(t.c22, kp.c22);
	cout << "Po dodaniu t + kp:\n"; print_matrix(t);

	//Zastosowanie funkcji FSBox(E, t)
	SBoxE(t.c11);
	SBoxE(t.c12);
	SBoxE(t.c21);
	SBoxE(t.c22);
	cout << "SBoxE:\n"; print_matrix(t);

	//Zastosowanie funkcji ZK(t)
	ZK(t);
	cout << "ZK:\n"; print_matrix(t);

	//Przemno�enie tekstu t przez ci�g bit�w m = (0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1): t = m � t
	matrix m{ 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1 };
	matrix temp{ t };
	t.c11 = dodawanie(mnozenie(m.c11, temp.c11), mnozenie(m.c12, temp.c21));
	t.c12 = dodawanie(mnozenie(m.c11, temp.c12), mnozenie(m.c12, temp.c22));
	t.c21 = dodawanie(mnozenie(m.c21, temp.c11), mnozenie(m.c22, temp.c21));
	t.c22 = dodawanie(mnozenie(m.c21, temp.c12), mnozenie(m.c22, temp.c22));
	cout << "Mnozenie m * t:\n"; print_matrix(t);

	//Dodawanie klucz rundy pierwsszej k1 do tekstu t: t = t + k1.
	t.c11 = dodawanie(t.c11, k1.c11);
	t.c12 = dodawanie(t.c12, k1.c12);
	t.c21 = dodawanie(t.c21, k1.c21);
	t.c22 = dodawanie(t.c22, k1.c22);
	cout << "Dodawanie klucza pierwszej rundy:\n"; print_matrix(t);

	//Zastosowanie funkcji SBoxE
	SBoxE(t.c11);
	SBoxE(t.c12);
	SBoxE(t.c21);
	SBoxE(t.c22);
	cout << "SBoxE:\n"; print_matrix(t);

	//Zastosowanie funkcji ZK(t)
	ZK(t);
	cout << "ZK:\n"; print_matrix(t);

	//Dodawanie klucz rundy pierwsszej k2 do tekstu t: t = t + k2.
	t.c11 = dodawanie(t.c11, k2.c11);
	t.c12 = dodawanie(t.c12, k2.c12);
	t.c21 = dodawanie(t.c21, k2.c21);
	t.c22 = dodawanie(t.c22, k2.c22);
	cout << "Dodawanie klucza drugiej rundy:\n"; print_matrix(t);
}

void deszyfrowanie(matrix &t)
{
	//mo�e kiedy�
}
int main()
{
	matrix t{ 0,0,1,1,1,1,0,0,1,1,0,0,0,0,1,1 };
	szyfrowanie(t);
	cout << "szyfrogram:\n";print_matrix(t);
	system("pause");
	return 0;
}