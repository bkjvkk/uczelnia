#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

vector<short> PW(vector<short> szyfr)
{
	vector<short> permutation = szyfr;
	szyfr[0] = permutation[1];
	szyfr[1] = permutation[5];
	szyfr[2] = permutation[2];
	szyfr[3] = permutation[0];
	szyfr[4] = permutation[3];
	szyfr[5] = permutation[7];
	szyfr[6] = permutation[4];
	szyfr[7] = permutation[6];
	return szyfr;
}

vector<short> P10(vector<short> kp)
{
	vector<short> permutation = kp;
	kp[0] = permutation[2];
	kp[1] = permutation[4];
	kp[2] = permutation[1];
	kp[3] = permutation[6];
	kp[4] = permutation[3];
	kp[5] = permutation[9];
	kp[6] = permutation[0];
	kp[7] = permutation[8];
	kp[8] = permutation[7];
	kp[9] = permutation[5];
	return kp;
}

vector<short> P8(vector<short> kp)
{
	vector<short> permutation = kp;
	kp.pop_back(); kp.pop_back();
	kp[0] = permutation[5];
	kp[1] = permutation[2];
	kp[2] = permutation[6];
	kp[3] = permutation[3];
	kp[4] = permutation[7];
	kp[5] = permutation[4];
	kp[6] = permutation[9];
	kp[7] = permutation[8];
	return kp;
}

vector<short> P4w8(vector<short> szyfr)
{
	vector<short> temps = szyfr;
	szyfr[0] = temps[3];
	szyfr[1] = temps[0];
	szyfr[2] = temps[1];
	szyfr[3] = temps[2];
	szyfr[4] = temps[1];
	szyfr[5] = temps[2];
	szyfr[6] = temps[3];
	szyfr[7] = temps[0];
	return szyfr;
}

vector<short> P4(vector<short> szyfr)
{
	vector<short> temps = szyfr;
	szyfr[0] = temps[1];
	szyfr[1] = temps[3];
	szyfr[2] = temps[2];
	szyfr[3] = temps[0];
	return szyfr;
}

vector<short> PO(vector<short> szyfr)
{
	vector<short> permutation = szyfr;
	szyfr[0] = permutation[3];
	szyfr[1] = permutation[0];
	szyfr[2] = permutation[2];
	szyfr[3] = permutation[4];
	szyfr[4] = permutation[6];
	szyfr[5] = permutation[1];
	szyfr[6] = permutation[7];
	szyfr[7] = permutation[5];
	return szyfr;
}

vector<short> XOR(vector<short> szyfr, vector<short> klucz)
{
	for (int i = 0;i < szyfr.size();i++)
	{
		if (szyfr[i] == 1 && klucz[i] == 0) szyfr[i] = 1;
		else if (szyfr[i] == 0 && klucz[i] == 1) szyfr[i] = 1;
		else if (szyfr[i] == 0 && klucz[i] == 0) szyfr[i] = 0;
		else if (szyfr[i] == 1 && klucz[i] == 1) szyfr[i] = 0;
		else szyfr[i] = 0;
	}
	return szyfr;
}

vector<short> sBox(vector<short> szyfr)
{
	short SBoxL[4][4] = { { 1,0,3,2 },{ 3,2,1,0 },{ 0,2,1,3 },{ 3,1,3,2 } };
	short SBoxR[4][4] = { { 0,1,2,3 },{ 2,0,1,3 },{ 3,0,1,0 },{ 2,1,0,3 } };
	int wiersz = szyfr[0] * 2 + szyfr[3] * 1;
	int kolumna = szyfr[1] * 2 + szyfr[2] * 1;
	int sprawdz = SBoxL[wiersz][kolumna];
	switch (sprawdz)
	{
	case 0:
		szyfr[0] = 0;
		szyfr[1] = 0;
		break;
	case 1:
		szyfr[0] = 0;
		szyfr[1] = 1;
		break;
	case 2:
		szyfr[0] = 1;
		szyfr[1] = 0;
		break;
	case 3:
		szyfr[0] = 1;
		szyfr[1] = 1;
		break;
	default:
		break;
	}

	wiersz = szyfr[4] * 2 + szyfr[7] * 1;
	kolumna = szyfr[5] * 2 + szyfr[6] * 1;
	sprawdz = SBoxR[wiersz][kolumna];
	switch (sprawdz)
	{
	case 0:
		szyfr[2] = 0;
		szyfr[3] = 0;
		break;
	case 1:
		szyfr[2] = 0;
		szyfr[3] = 1;
		break;
	case 2:
		szyfr[2] = 1;
		szyfr[3] = 0;
		break;
	case 3:
		szyfr[2] = 1;
		szyfr[3] = 1;
		break;
	default:
		break;
	}

	for (int i = 0;i < 4;i++)
		szyfr.pop_back();

	return szyfr;
}

vector<short> przesuniecie(vector<short> kp, int start, int end)
{
	int zmiana = kp[start];
	for (int i = start;i < end;i++) 
		kp[i] = kp[i + 1];
	kp[end] = zmiana;
	return kp;
}

vector<short> krzyzowanie(vector<short> szyfr)
{
	vector<short> temps = szyfr;
	szyfr[0] = temps[4];
	szyfr[1] = temps[5];
	szyfr[2] = temps[6];
	szyfr[3] = temps[7];
	szyfr[4] = temps[0];
	szyfr[5] = temps[1];
	szyfr[6] = temps[2];
	szyfr[7] = temps[3];
	return szyfr;
}

vector<short> szyfrowanie(vector<short> szyfr, vector<short> kp)
{
	vector<short> szyfr2;
	vector<short> kopia;
	for (int i = 4;i < 8;i++)
		szyfr2.push_back(szyfr[i]);
	kopia = szyfr2;
	for (int i = 0;i < 4;i++)
	{
		szyfr2.push_back(0);
		szyfr.pop_back();
	}

	szyfr2 = P4w8(szyfr2);
	szyfr2 = XOR(szyfr2, kp);
	szyfr2 = sBox(szyfr2);
	szyfr2 = P4(szyfr2);
	szyfr = XOR(szyfr, szyfr2);
	for (int x : kopia) szyfr.push_back(x);
	return szyfr;
}

int main()
{
	char temp;
	vector<short> szyfr;
	vector<short> szyfr2;
	vector<short> kopia;
	vector<short> kp = { 1,0,1,0,0,0,0,0,1,0 };
	vector<short> kp2 = kp;
	//wczytywanie znak�w
	cout << "Podaj bity do zaszyfrowania (max 8) lub 'x' by przerwac wczytywanie znakow: ";
	while (cin >> temp)
	{
		if (temp == '0' || temp == '1')
			szyfr.push_back(temp - '0');
		else if (temp == 'x') break;
		else cout << "Niewlasciwy znak!\n";

		if (szyfr.size() == 8) break;
	}
	//sprawdzanie d�ugo�ci
	if (szyfr.size() < 8)
		for (int i = szyfr.size() - 1; i < 7; ++i)
			szyfr.push_back(0);
	for (int x : szyfr) cout << x;
	
	//obliczanie kluczy
	kp = P10(kp);
	kp = przesuniecie(kp, 0, 4);
	kp = przesuniecie(kp, 5, 9);
	kp2 = kp;
	kp2 = przesuniecie(kp2, 0, 4);
	kp2 = przesuniecie(kp2, 0, 4);
	kp2 = przesuniecie(kp2, 5, 9);
	kp2 = przesuniecie(kp2, 5, 9);
	kp = P8(kp);
	kp2 = P8(kp2);
	cout << endl << "kp1: ";
	for (int x : kp) cout << x;
	cout <<endl << "kp2: " ;
	for (int x : kp2) cout << x;
	
	//Szyfrowanie 
	szyfr = PW(szyfr);
	szyfr = szyfrowanie(szyfr,kp);
	szyfr = krzyzowanie(szyfr);
	szyfr = szyfrowanie(szyfr, kp2);
	szyfr = PO(szyfr);
	cout << endl << "Finalny szyfr: ";
	for (int x : szyfr) cout << x;

	//Deszyfrowanie
	szyfr = PW(szyfr);
	szyfr = szyfrowanie(szyfr, kp2);
	szyfr = krzyzowanie(szyfr);
	szyfr = szyfrowanie(szyfr, kp);
	szyfr = PO(szyfr);
	cout << endl << "Odszyfrowany szyfr: ";
	for (int x : szyfr) cout << x;
	cout << endl;
	system("pause");
	return 0;
}