#include "helper_functions.hpp"

bool check_input(int n, int r, string &error, vector<int> &sitko)
{
	srand(time(NULL));

	//spradzanie pierwszosci lukasem
	/*int count_true=0; //przeprowadzamy test 10x i sprawdzamy wi�kszo�cia wynik�w
	for (int i = 0; i < 10; i++)
	{
		if(test_lucasa(n, rand() % (n - 3) + 2)) count_true++;
	}
	if (!(count_true >= 5)) { error = "liczba n prawdopodobnie nie jest pierwsza\n"; return false; }*/
	
	//sprawdzanie sitem (sito generuje w mainie i tylko refka do funkcji)
	for (int i = 0;i < sitko.size();i++)
	{
		if (sitko[i] < n) continue;
		else if (sitko[i] == n) break;
		else if (sitko[i] > n)
		{
			error = "liczba n nie jest na liscie liczb pierwszych\n";
			break;
		}
	}

	Counting_vec andrzej;
	fermata(n - 1, 0, andrzej);
	if (andrzej.num_of_elements() > 1) andrzej.sort();
	for (int i = 0; i < andrzej.num_of_elements(); i++)
	{
		if (s_p_m(r, (n - 1) / andrzej.get(i), n) == 1)
		{
			error = "r nie jest pierwiastkiem pierwotnym n!\n";
			return false;
		}
	}
	return true;
}

int main()
{

	vector<int> sitko;
	sitko = sito(50000);

	fstream odczyt, raport;
	odczyt.open("dane.txt", ios::in);
	if (odczyt.good() == true)
	{
		raport.open("raport.txt", ios::out);
		cout << "Uzyskano dostep do pliku!" << endl;
		
		do
		{
			int n, r, k,j,t,c1,c2;
			if (!(odczyt >> n >> r >> k >> j >> t))
			{
				raport << "Koniec pliku!\n";
				break;
			}
			string error;
			if (check_input(n, r,error,sitko))
			{
				int a = s_p_m(r, k, n);
				c1 = s_p_m(r, j, n);
				c2 = (t * s_p_m(a, j, n)) % n;
				int todsz = (c2 * s_p_m(c1, n - 1 - k, n)) % n;
				raport << "n: " << n << ", r: " << r << ", k: " << k << endl;
				raport << "klucz publiczny : (" << n << ", " << r << ", " << a << ")\n";
				raport << "j: " << j << ", t: " << t << endl;
				raport << "klucz prywatny : (" << c1 << ", " << c2 << ")\n";
				raport << "odszyfrowany tekst: " << todsz << endl;

			}
			else
			{
				raport << "n: " << n << ", r: " << r << ", k: " << k << ", j: " << j << ", t: " << t << endl;
				raport << error << endl;
			}
			raport << endl << endl;
		} while (true);
	}
	else cout << "Dostep do pliku zostal zabroniony!" << std::endl;
	
	cout << sitko[sitko.size() - 1] << endl;
	system("Pause");
	return 0;
}