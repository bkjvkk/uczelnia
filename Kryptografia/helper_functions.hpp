#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>

using namespace std;

//srand(time(NULL));
// int x = rand() % liczba;

void swap_vec(vector<int> &a)
{
	vector<int> toswap;
	for (int i = a.size() - 1;i >= 0;i--) toswap.push_back(a[i]);
	a = toswap;
}
vector<int> dec_to_bin(int a)
{
	vector<int> wynik;
	while (a > 0)
	{
		wynik.push_back(a % 2);
		a /= 2;
	}
	for (int i = wynik.size(); i < 4;i++) wynik.push_back(0);
	swap_vec(wynik);
	return wynik;
}
int bin_to_dec(vector<int> a)
{
	int liczba = 0;
	swap_vec(a);
	for (int i = 0;i < a.size();i++) if (a[i] == 1) liczba += pow(2, i);
	return liczba;
}

//vector kt�ry zapami�tuje ilo�� wyst�pie� danej liczby ca�kowitej
struct Counting_vec {
	Counting_vec() {};
	Counting_vec(Counting_vec &ref) { data = ref.data; repeating = ref.repeating; };
	void sort();
	void add(int a);
	void print();
	int num_of_elements() { return data.size(); };
	int get(int i) { if (i < 0 || i >= data.size()) return 0; else return data[i]; };
private:
	vector<int> data;
	vector<int> repeating;
};
void Counting_vec::add(int a)
{
	bool exist = false;
	for (int i = 0;i < data.size();i++)
	{
		if (data[i] == a)
		{
			repeating[i]++;
			exist = true;
		}
	}
	if (!exist)
	{
		data.push_back(a);
		repeating.push_back(1);
	}
}
void Counting_vec::print()
{
	for (int i = 0;i < data.size();i++)
	{
		cout << data[i] << " appeared " << repeating[i] << " time/s in task\n";
	}
}
void Counting_vec::sort()
{
	
	if (data.size() >= 2)
	{
		for (int i = 0;i < data.size();i++)
		{
			if (i == data.size()) break;
			int swapper[3] = { data[i], repeating[i], i };

			for (int j = i;j < data.size();j++)
			{
				if (j == data.size()) break;
				if (swapper[0] > data[j])
				{
					swapper[0] = data[j];
					swapper[1] = repeating[j];
					swapper[2] = j;
				}
			}

			if (swapper[2] != i)
			{
				data[swapper[2]] = data[i]; //data[j] = data[i]
				repeating[swapper[2]] = repeating[i]; //repeating[j] = repeating[i]
				data[i] = swapper[0]; //data[i] = data[j]
				repeating[i] = swapper[1]; //repeating[i] = reapeating[j]
			}
		}
	}
	else std::cout << "There is nothing to sort!\n";

}

vector<int> sito(int n)
{
	if (n <= 1) return{ 0 };
	vector<int> vec;
	for (int i = 2; i < n; i++)
		vec.push_back(i);

	int i = 0;
	while (vec[i] <= sqrt(n))
	{
		vector<int> temp;
		for (int j = 0; j <= i; j++) temp.push_back(vec[j]);
		for (int j = i + 1; j < vec.size(); j++)
			if ((vec[j] % vec[i]) != 0) temp.push_back(vec[j]);
		vec = temp;
		i++;
	}

	return vec;
}

//temp

int s_p_m(int a, int b, int c)
{
	vector<int> b_bin;
	unsigned long long int iloczyn = 1;
	unsigned long long int lucas = a;
	b_bin = dec_to_bin(b);
	swap_vec(b_bin);
	for (int i = 0; i < b_bin.size(); i++)
	{
		if (b_bin[i] == 1)
		{
			iloczyn *= lucas;
			iloczyn %= c;
		}

		lucas *= lucas;
		lucas %= c;
	}
	return (iloczyn % c);
}

bool fermata(int a, int flag, Counting_vec &lp) //flag 0 - calculate begining 1 - dont
{
	int d = a;
	if (flag == 0)
	{
		int pow_of_2 = 2;
		do
		{
			a = d / pow_of_2;
			//cout << d << " : " << a << endl;
			pow_of_2 *= 2;
			lp.add(2);
		} while ((a % 2) == 0 && a != 0);
	}
	double x;
	int y;
	x = int(sqrt(a));
	if (x != sqrt(a)) x++;
	//cout << d << " ,x : " << x << endl;

	while (x < (a + 1) / 2)
	{
		y = x*x - a;
		//cout << d << " ,y^2: " << y << endl;
		if (int(sqrt(y)) != sqrt(y)) x++;
		else
		{
			y = sqrt(y);
			//cout << x + y << " " << x - y << endl;
			if (!fermata(x + y, 1, lp)) 	lp.add(x + y);
			if (!fermata(x - y, 1, lp)) lp.add(x - y);
			return true;
		}
	}
	//cout << "Nie znaleziono rozkladu dla d = " << d << endl;
	return false;
}

bool test_lucasa(int a, int q)
{
	Counting_vec lp;
	fermata(a, 0, lp);
	if(lp.num_of_elements() > 1) lp.sort();
	vector<int> test;
	bool czy_przeszla = true;
	if (s_p_m(q, a - 1, a) == 1)
	{
		for (int i = 0; i < lp.num_of_elements(); i++)
		{
			//cout << lp.get(i) << endl;
			test.push_back(s_p_m(q, (a - 1) / lp.get(i), a));
		}
		for (int i : test)
			if (i == 1)
				czy_przeszla = false;

		//if (czy_przeszla) cout << "Liczba " << a << " przeszla test lukasa\n";
		//else cout << "Liczba " << a << " nie przeszla testu lukasa\n";
		return czy_przeszla;
	}
	//else cout << "Liczba " << a << " nie przeszla testu pierwszosci lukasa, sorki\n";
	return false;
}