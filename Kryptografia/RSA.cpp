#include "helper_functions.hpp"
vector<int> sito(int n)
{
	if (n <= 1) return { 0 };
	vector<int> vec;
	for (int i = 2; i < n;i++)
		vec.push_back(i);

	int i = 0;
	while (vec[i] <= sqrt(n))
	{
		vector<int> temp;
		for (int j = 0; j <= i;j++) temp.push_back(vec[j]);
		for (int j = i + 1; j < vec.size(); j++)
			if ((vec[j] % vec[i]) != 0) temp.push_back(vec[j]);
		vec = temp;
		i++;
	}

	return vec;
}

vector<int> euklides(int a, int b, vector<int> &tab_x)
{

	int r0 = a;
	int r1 = b;
	int x1 = 1, x2;
	int y1 = -(r0 / r1), y2;
	int setup = 1;

	while (r1 != 0)
	{
		int q = r0 / r1;
		int rj = r0 - r1 * q;
		r0 = r1;
		r1 = rj;
		
		if (setup == 2)
		{
			x2 = -(x1*q);
			y2 = 1 - (y1 * q);
		}
		else if (setup >= 3)
		{
			int x = x1 - (x2 * q);
			int y = y1 - (y2 * q);

			x1 = x2;
			x2 = x;

			y1 = y2;
			y2 = y;
		}
		tab_x.push_back(x1);
		setup++;
	}
	return { r0,x1,y1 };
}

void RSA()
{
	srand(time(NULL));
	vector<int> primary_numbers;
	vector<int> NWD;
	vector<int> X;
	int e;
	int d = 0;
	primary_numbers = sito(20000); //incjalizacja 2200 liczb losowych

	//int p = rand() % 1200 + 1000; //wybieranie p i q
	//int q = rand() % 1200 + 1000;
	//p = primary_numbers[p];
	//q = primary_numbers[q]; 
	int p = primary_numbers[320];
	int q = primary_numbers[122];
	cout << "p: " << p << " q: " << q << endl;

	unsigned long long int n = p * q; //generacja n i m
	unsigned long long int m = (p - 1) * (q - 1);
	cout << "n: " << n << " m: " << m << endl;

	do									//wybieranie e
	{
		vector<int> temp;
		//e = (rand() % m) + 1;
		e = 7;
		NWD = euklides(e, m,temp);
		X = temp;
	} while (NWD[0] != 1);
	cout << "NWD: " << NWD[0] << " e: " << e << endl;

	/*for (int i = X.size() - 1;i >= 0;i--)
	{
		cout << "X" << i << ": " << X[i] << endl;
		if (X[i] > 0)
		{
			d = X[i];
			break;
		}
	}*/
	if (NWD[1] > 0) d = NWD[1];
	else
	{
		NWD[1] +=m;
		if (NWD[1] > 0) d = NWD[1];
	}
	cout << "d: " << d << endl;

	cout << "Klucz publiczny (" << n << ", " << e << ")" << endl;
	cout << "Klucz prywatny (" << n << ", " << d << ")" << endl;
}
int main()
{
	//vector<int> sitko;
	//sitko = sito(20000);
	//for (auto x : sitko) cout << x << " ";
	//cout << sitko.size() << endl;

	//vector<int> nwd;
	//nwd = euklides(8280, 990);
	//for (auto x : nwd) cout << x << " ";
	//cout << endl;

	RSA();
	system("pause");
	return 0;
}