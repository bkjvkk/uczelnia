#include "helper_functions.hpp"

int s_p_m(int a, int b, int c)
{
	vector<int> b_bin;
	unsigned long long int iloczyn = 1;
	unsigned long long int lucas = a;
	b_bin = dec_to_bin(b);
	swap_vec(b_bin);
	for (int i = 0;i < b_bin.size();i++)
	{
		if (b_bin[i] == 1)
		{
			iloczyn *= lucas;
			iloczyn %= c;
		}

		lucas *= lucas;
		lucas %= c;
	}
	return (iloczyn % c);
}

bool fermata(int a,int flag, Counting_vec &lp) //flag 0 - calculate begining 1 - dont
{
	int d = a;
	if (flag == 0)
	{
		int pow_of_2 = 2;
		do
		{
			a = d / pow_of_2;
			//cout << d << " : " << a << endl;
			pow_of_2 *= 2;
			lp.add(2);
		} while ((a % 2) == 0 && a != 0);
	}
	double x;
	int y;
	x = int(sqrt(a));
	if (x != sqrt(a)) x++;
	//cout << d << " ,x : " << x << endl;

	while (x < (a + 1) / 2)
	{
		y = x*x - a;
		//cout << d << " ,y^2: " << y << endl;
		if (int(sqrt(y)) != sqrt(y)) x++;
		else
		{
			y = sqrt(y);
			//cout << x + y << " " << x - y << endl;
			if (!fermata(x + y, 1, lp)) 	lp.add(x + y);
			if (!fermata(x - y, 1, lp)) lp.add(x - y);
			return true;
		}
	}
	//cout << "Nie znaleziono rozkladu dla d = " << d << endl;
	return false;
}

void test_lucasa(int a, int q)
{
	Counting_vec lp;
	fermata(a, 0, lp);
	lp.sort();
	vector<int> test;
	bool czy_przeszla = true;
	if (s_p_m(q, a - 1, a) == 1)
	{
		for (int i = 0; i < lp.num_of_elements();i++)
		{
			//cout << lp.get(i) << endl;
			test.push_back(s_p_m(q, (a - 1) / lp.get(i), a));
		}
		for(int i:test)
			if (i == 1)
				czy_przeszla = false;

		if(czy_przeszla) cout << "Liczba " << a << " przeszla test lukasa\n";
		else cout << "Liczba " << a << " nie przeszla testu lukasa\n";
	}
	else cout << "Liczba " << a << " nie przeszla testu pierwszosci lukasa, sorki\n";
}
int main()
{
	//cout << s_p_m(2234, 1234567, 9876) << endl;
	/*Counting_vec liczby_pierwsze;
	fermata(23321,0,liczby_pierwsze);
	liczby_pierwsze.sort();
	liczby_pierwsze.print();*/
	//test_lucasa(23321, 2223);
	system("pause");
	return 0;
}