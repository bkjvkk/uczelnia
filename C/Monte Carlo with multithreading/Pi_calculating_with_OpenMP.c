    /*
    area of square: a1 = (2*RADIUS)^2 ~ 4 * RADIUS^2  -> RADIUS^2 = a1 / 4
    area of circle: a2 = Pi*RADIUS^2                  -> RADIUS^2 = a2 / Pi
    a1 / 4 = a2 / Pi    -> a1 / 4 * a2 = 1/Pi -> Pi = 4* a2/a1

    Our monte-carlo algorithm will "randomly" generate cordinates x and y (from the area of the square)
    and check wheather the same cordinates belongs to the area of the circle.
    a2 - Number of points in the square (all points generated should be in this area)
    a1 - Number of points in the circle
    */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <limits.h>

#define  NUM_LOOPS 2000000
#define RADIUS 16.0

float rand_float(float maximum, int *seed)
{
  //copy-paste definition of rand_r
  unsigned int next = *seed;
      int result;

      next *= 1103515245;
      next += 12345;
      result = (unsigned int) (next / 65536) % 2048;

      next *= 1103515245;
      next += 12345;
      result <<= 10;
      result ^= (unsigned int) (next / 65536) % 1024;

      next *= 1103515245;
      next += 12345;
      result <<= 10;
      result ^= (unsigned int) (next / 65536) % 1024;

      *seed = next;
      //printf("%d == %d\n", *seed, result);
      //returned value should be from <-radius;radius>
  return (((float)result/(float)(INT_MAX)) * 2 * maximum) - maximum;
}

int main()
{
    int a2 = 0, a1 = 0, setup = 1, seed = 0;
    float pi,x,y;
    time_t begin;
    long i;
    begin = time(NULL);

    #pragma omp parallel shared(a2, a1) private(i, x, y, seed)
    {
      seed = omp_get_thread_num();

      #pragma omp for
      for(i = 0; i < NUM_LOOPS; i++)
      {
        //we generate two cordinates and check wheather or not they are in the area of circle
        x = rand_float(RADIUS, &seed);
        y = rand_float(RADIUS, &seed);
        if((x*x) + (y*y) <= RADIUS*RADIUS)
        {
          #pragma omp critical
          a1++;
        }
        #pragma omp critical
        a2++;
        //printf("id: %d\tx: %f\t y: %f\n", omp_get_thread_num(), x,y);
      }
    }
    pi = 4.0*((float)a1/(float)a2);
    printf("Total points: %d\nPoints in circle: %d\nApproximate PI: %f\n",a2,a1,pi);
    
    begin = time(NULL) - begin;
    printf ("It took me %f seconds).\n",((float)begin));
    system("pause");
    return 0;
}