#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <pthread.h>

//different sides of pipe
#define WRITE 1
#define READ 0

int main ()
{
  int ppc[2]; //[P]ipe [P]arent to [C]hild

  int len[3], x1, x2, x3, i, result, status = 0;
  pid_t pid;
  pthread_t thread1;

//Create communication between child and parent
    if (pipe (ppc) < 0)
    {
      perror ("Failed to allocate pipes");
      exit (EXIT_FAILURE);
    }

//creating child process
    if ((pid = fork ()) < 0)	//error child could not be created
    {
      perror ("Failed to fork process");
      return EXIT_FAILURE;
    }
    
    if (pid == 0)
    {
        printf ("Child");

        pid = getpid ();
        //get data from parent
        len[0] = read (ppc[READ], &x1, sizeof (x1));
        len[1] = read (ppc[READ], &x2, sizeof (x2));
        len[2] = read (ppc[READ], &x3, sizeof (x3));
        for(i = 0; i < 3; i++)
        {
            if (len[i] < 0)
	        {
	            perror ("Child: Failed to read data from pipe");
	            exit (EXIT_FAILURE);
	        }
            else if (len[i] == 0)
	        {
	            fprintf (stderr, "Child: Read [%d] EOF from pipe\n", i);
	        }
	        else
	        {
	            printf("Child: Read [%d] value\n", i);
	        }
        }
	    printf ("Child(%d): Received %d %d %c\n", pid, x1, x2, x3);

	    switch ((char) x3)
	    {
	        case '-':
	            result = x1 - x2;
	            break;
	        case '+':
	            result = x1 + x2;
	            break;
	        case '*':
	            result = x1 * x2;
	            break;
	        case '/':
	            result = x1 / x2;
	            break;
	        default:
	            printf ("wrong sign\n");
	            break;
	       }

	        printf ("Child(%d): Sending %d back\n", pid, result);

        close (ppc[READ]);
        if (write (ppc[WRITE], &result, sizeof (result)) != sizeof (result))
        {
            perror ("1: Parent: Failed to send value to child ");
            exit (EXIT_FAILURE);
        }
        close (ppc[WRITE]);
        
        return EXIT_SUCCESS;
    }
    else 
    {

      pid = getpid ();

      x1 = 42;
      x2 = 10;
      x3 = (int) '-';

      printf ("Parent(%d): Sending %d, %d, %c to child\n", pid, x1, x2, x3);

      //send data to child
      if (write (ppc[WRITE], &x1, sizeof (x1)) != sizeof (x1) || 
          write (ppc[WRITE], &x2, sizeof (x2)) != sizeof (x2) ||
          write (ppc[WRITE], &x3, sizeof (x3)) != sizeof (x3))
      {
        perror ("1: Parent: Failed to send value to child ");
        exit (EXIT_FAILURE);
      }
      //wait for child processes to end
      while ((pid = wait(&status)) > 0);

      //read data from child
      len[0] = read(ppc[READ], &result, sizeof (result));
      if (len[0] < 0)
            {
                perror ("Parent: Failed to read data from pipe");
                exit (EXIT_FAILURE);
            }
              else if (len[0] == 0)
            {
                fprintf (stderr, "Parent: Read EOF from pipe\n", i);
            }
            else
            {
                printf("Parent: Read value  %d\n", result);
            }
      close (ppc[READ]);
    
    }
  return EXIT_SUCCESS;
}
